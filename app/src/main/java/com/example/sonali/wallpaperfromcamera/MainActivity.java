package com.example.sonali.wallpaperfromcamera;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button wallButton,takePhotoButton;
    private ImageView pickImage;
    int cameraData = 0;
    Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        InputStream inputStream  = getResources().openRawResource(R.raw.ic_launcher_foreground);
        wallButton = findViewById(R.id.bset);
        takePhotoButton = findViewById(R.id.btake);
        bitmap = BitmapFactory.decodeStream(inputStream);
        pickImage =  findViewById(R.id.image);

        wallButton.setOnClickListener(this);
        takePhotoButton.setOnClickListener(this);
    }


    public void onClick(View v){
        switch(v.getId()){
            case R.id.bset:
                try{
                    getApplicationContext().setWallpaper(bitmap);
                    Toast.makeText(getApplicationContext(), "Wallpaper Set",Toast.LENGTH_LONG).show();
                } catch (IOException e){
                    e.printStackTrace();
                }
                break;

            case R.id.btake:
                Intent intent =  new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, cameraData);
                break;
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){
            Bundle extras = data.getExtras();
            bitmap = (Bitmap) extras.get("data");
            pickImage.setImageBitmap(bitmap);

        }
     }
}

